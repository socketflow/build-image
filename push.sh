#!/bin/sh

echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY || exit 1
docker build -t $CI_REGISTRY/socketflow/build-image:$CI_COMMIT_SHORT_SHA . || exit 1
docker push $CI_REGISTRY/socketflow/build-image:$CI_COMMIT_SHORT_SHA || exit 1