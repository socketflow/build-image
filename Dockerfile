# Modify the arguments below to update software versions.

ARG UBUNTU_VERSION=21.04

# Start with Ubuntu.
FROM ubuntu:${UBUNTU_VERSION}

ARG RUST_VERSION=1.54.0
ARG TERRAFORM_VERSION=1.0.3
ARG GOLANG_VERSION=1.16.6
ARG NODE_VERSION=14.17.4

# Install some needed packages.
RUN apt-get update
RUN apt-get install -y git unzip zip curl wget gcc xz-utils

# Install Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# Install Terraform.
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN mv terraform /usr/bin
RUN rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# Install Go.
RUN wget https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz
RUN tar -xf go${GOLANG_VERSION}.linux-amd64.tar.gz
RUN mv go /usr/bin
RUN rm go${GOLANG_VERSION}.linux-amd64.tar.gz
ENV PATH="/usr/bin/go/bin:$PATH"

# Install Go tooling to support local Go extension.
RUN go get github.com/uudashr/gopkgs/v2/cmd/gopkgs
RUN go get github.com/ramya-rao-a/go-outline
RUN go get github.com/cweill/gotests
RUN go get github.com/fatih/gomodifytags
RUN go get github.com/josharian/impl
RUN go get github.com/haya14busa/goplay/cmd/goplay
RUN go get github.com/go-delve/delve/cmd/dlv
RUN go get honnef.co/go/tools/cmd/staticcheck
RUN go get golang.org/x/tools/gopls

# Install Node.
RUN wget https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz
RUN tar -xf node-v${NODE_VERSION}-linux-x64.tar.xz
RUN mv node-v${NODE_VERSION}-linux-x64 /usr/bin/node
RUN rm node-v${NODE_VERSION}-linux-x64.tar.xz
ENV PATH="/usr/bin/node/bin:$PATH"
